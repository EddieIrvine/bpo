<?php

namespace App\DataFixtures;


use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class DataFixtures extends Fixture
{
    private function fillEmployeesAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 1000; $i++)
        {
            $employee = new Employee();

            $employee->setFirstName($faker->firstName);
            $employee->setLastName($faker->lastName);
            $employee->setPosition($faker->sentence(4));
            $employee->setReward($faker->randomNumber(4));
            $employee->setHireDate($faker->dateTimeThisCentury);

            $entityManager->persist($employee);
            $entityManager->flush();
        }
    }

    private function fillProjectsAction()
    {
        $entityManger = $this->getDoctrine()->getManager();
        $employees = $this->getDoctrine()
            ->getRepository(Employee::class)
            ->findAll();

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 200; $i++)
        {
            $project = new Project();

            $project->setName($faker->sentence(2));

            $start = $faker->dateTimeThisDecade();

            do
            {
                $end = $faker->dateTimeThisDecade();
            } while ($end < $start);

            $project->setStartDate($start);
            $project->setStopDate($end);

            $project->setCost($faker->randomNumber(6));
            $project->setExecutionProgress($faker->randomFloat(4, 0, 100));
            $project->setAbout($faker->text);

            shuffle($employees);

            $project->setSupervisor($employees[0]->getId());

            $entityManger->persist($project);
            $entityManger->flush();
        }
    }

    private function fillMembersAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $faker = Faker\Factory::create();

        $employees = $this->getDoctrine()
            ->getRepository(Employee::class)
            ->findAll();

        $projects = $this->getDoctrine()
            ->getRepository(Project::class)
            ->findAll();

        foreach ($projects as $project)
        {
            $member = new Member();

            $member->setEmployeeID($project->getSupervisor());
            $member->setProjectID($project->getId());
            $member->setFunction($faker->word);
            $member->setExtra($faker->randomNumber(3));
            $member->setGrade($faker->randomNumber(1) + 1);

            $entityManager->persist($member);
            $entityManager->flush();
        }

        for ($i = 0; $i < 500; $i++)
        {
            $member = new Member();

            do
            {
                shuffle($employees);
                shuffle($projects);

                $eId = $employees[0]->getId();
                $pId = $projects[0]->getId();

                $res = $this->getDoctrine()
                    ->getRepository(Member::class)
                    ->findOneBy([
                        'employeeID' => $eId,
                        'projectID' => $pId
                    ]);

            } while (!empty($res));

            $member->setEmployeeID($eId);
            $member->setProjectID($pId);
            $member->setFunction($faker->word);
            $member->setExtra($faker->randomNumber(3));
            $member->setGrade($faker->randomNumber(1) + 1);

            $entityManager->persist($member);
            $entityManager->flush();
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->fillEmployeesAction();
        $this->fillProjectsAction();
        $this->fillMembersAction();
    }
}