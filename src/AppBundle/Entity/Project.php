<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="Projekty")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_projektu", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwa", type="string", length=50)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_rozpoczecia", type="date")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_zakonczenia", type="date", nullable=true)
     */
    private $stopDate;

    /**
     * @var double
     *
     * @ORM\Column(name="koszt", type="float")
     */
    private $cost;

    /**
     * @var string
     *
     * @ORM\Column(name="opis", type="text", nullable=true)
     */
    private $about;

    /**
     * @var double
     *
     * @ORM\Column(name="procent_wykonania", type="float")
     */
    private $executionProgress;

    /**
     * @var int
     *
     * @ORM\Column(name="kierownik", type="integer")
     * @ORM\ManyToOne(targetEntity="Employee", inversedBy="Projekty")
     * @ORM\JoinColumn(name="kierownik", referencedColumnName="id_pracownika")
     */
    private $supervisor;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Project
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set stopDate
     *
     * @param \DateTime $stopDate
     *
     * @return Project
     */
    public function setStopDate($stopDate)
    {
        $this->stopDate = $stopDate;

        return $this;
    }

    /**
     * Get stopDate
     *
     * @return \DateTime
     */
    public function getStopDate()
    {
        return $this->stopDate;
    }

    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return Project
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return Project
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set executionProgress
     *
     * @param double $executionProgress
     *
     * @return Project
     */
    public function setExecutionProgress($executionProgress)
    {
        $this->executionProgress = $executionProgress;

        return $this;
    }

    /**
     * Get executionProgress
     *
     * @return double
     */
    public function getExecutionProgress()
    {
        return $this->executionProgress;
    }

    /**
     * Set supervisor
     *
     * @param integer $supervisor
     *
     * @return Project
     */
    public function setSupervisor($supervisor)
    {
        $this->supervisor = $supervisor;

        return $this;
    }

    /**
     * Get supervisor
     *
     * @return int
     */
    public function getSupervisor()
    {
        return $this->supervisor;
    }
}

