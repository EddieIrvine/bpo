<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Member
 *
 * @ORM\Table(name="Uczestnicy")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MemberRepository")
 */
class Member
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="id_pracownika", type="integer")
     * @ORM\ManyToOne(targetEntity="Employee", inversedBy="Uczestnicy")
     * @ORM\JoinColumn(name="id_pracownika", referencedColumnName="id_pracownika")
     */
    private $employeeID;

    /**
     * @var int
     *
     * @ORM\Column(name="id_projektu", type="integer")
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="Uczestnicy")
     * @ORM\JoinColumn(name="id_projektu", referencedColumnName="id_projektu")
     */
    private $projectID;

    /**
     * @var string
     *
     * @ORM\Column(name="funkcja", type="string", length=40)
     */
    private $function;

    /**
     * @var int
     *
     * @ORM\Column(name="premia", type="integer", nullable=true)
     */
    private $extra;

    /**
     * @var int
     *
     * @ORM\Column(name="ocena", type="integer", nullable=true)
     */
    private $grade;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employeeID
     *
     * @param integer $employeeID
     *
     * @return Member
     */
    public function setEmployeeID($employeeID)
    {
        $this->employeeID = $employeeID;

        return $this;
    }

    /**
     * Get employeeID
     *
     * @return int
     */
    public function getEmployeeID()
    {
        return $this->employeeID;
    }

    /**
     * Set projectID
     *
     * @param integer $projectID
     *
     * @return Member
     */
    public function setProjectID($projectID)
    {
        $this->projectID = $projectID;

        return $this;
    }

    /**
     * Get projectID
     *
     * @return int
     */
    public function getProjectID()
    {
        return $this->projectID;
    }

    /**
     * Set function
     *
     * @param string $function
     *
     * @return Member
     */
    public function setFunction($function)
    {
        $this->function = $function;

        return $this;
    }

    /**
     * Get function
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * Set extra
     *
     * @param integer $extra
     *
     * @return Member
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra
     *
     * @return int
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set grade
     *
     * @param integer $grade
     *
     * @return Member
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return int
     */
    public function getGrade()
    {
        return $this->grade;
    }
}

