<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Entity\Employee;

class EmployeesController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        $qb = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->from('AppBundle:Employee', 'e')
            ->select('e');

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $qb,
            $request->query->get('page', 1),
            20
        );

        return $this->render('default/index.html.twig', [
            'employees' => $pagination
        ]);
    }

    /**
     * @Route("/add", name="add_employee")
     */
    public function addAction(Request $request)
    {
        $employee = new Employee();

        $employee->setHireDate(new \DateTime());

        $form = $this->createFormBuilder($employee)
            ->add('first_name', TextType::class, ['label' => 'Imię'])
            ->add('last_name', TextType::class, ['label' => 'Nazwisko'])
            ->add('position', TextType::class, ['label' => 'Stanowisko'])
            ->add('reward', NumberType::class, ['label' => 'Wynagrodzenie'])
            ->add('hire_date', DateType::class, [
                'label' => 'Data zatrudnienia (rok, miesiąc, dzień)',
                'format' => 'yyyy MM dd'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Dodaj pracownika',
                'attr' => ['class' => 'btn btn-success']
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid())
        {
            $employee = $form->getData();

            $entityManager = $this->getDoctrine()
                ->getManager();

            $entityManager->persist($employee);
            $entityManager->flush();

            $this->addFlash('success', 'Pracownik został pomyślnie dodany');

            return $this->redirectToRoute('home');
        }

        return $this->render('default/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit_employee")
     */
    public function editAction(Employee $employee, Request $request)
    {
        $form = $this->createFormBuilder($employee)
            ->add('first_name', TextType::class, ['label' => 'Imię'])
            ->add('last_name', TextType::class, ['label' => 'Nazwisko'])
            ->add('position', TextType::class, ['label' => 'Stanowisko'])
            ->add('reward', NumberType::class, ['label' => 'Wynagrodzenie'])
            ->add('hire_date', DateType::class, [
                'label' => 'Data zatrudnienia (rok, miesiąc, dzień)',
                'format' => 'yyyy MM dd'
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Zatwierdź',
                'attr' => ['class' => 'btn btn-success']
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() and $form->isValid())
        {
            $employee = $form->getData();

            $entityManager = $this->getDoctrine()
                ->getManager();

            $entityManager->persist($employee);
            $entityManager->flush();

            $this->addFlash('success', 'Pomyślnie zmieniono dane pracownika!');

            return $this->redirectToRoute('home');
        }

        return $this->render('default/add.html.twig', [
            'form' => $form->createView(),
            'employee' => $employee
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete_employee")
     */
    public function deleteAction(Employee $employee, Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('yes', SubmitType::class, [
                'label' => 'Tak',
                'attr' => ['class' => 'btn btn-success']
            ])
            ->add('no', SubmitType::class, [
                'label' => 'Nie',
                'attr' => ['class' => 'btn btn-danger']
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            if ($form->get('yes')->isClicked())
            {
                $em = $this->getDoctrine()
                    ->getManager();

                $em->remove($employee);
                $em->flush();

                $this->addFlash('success', 'Usunięto pracownika!');
            }

            return $this->redirectToRoute('home');
        }

        return $this->render('default/delete.html.twig', [
            'form' => $form->createView()
        ]);
    }

}

